#python -m pip install -U pygame --user
#python -m pygame.examples.aliens <-- zu testen

# import the pygame module, so you can use it
import pygame, sys
from pygame.locals import *
from GameTree import Node
from board import Board

 

# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    # call the main function
    main()



    # # initialize the pygame module
    # pygame.init()
    # # load and set the logo
    # logo = pygame.image.load("logo32x32.png")
    # board = pygame.image.load("board.png")
    # pygame.display.set_icon(logo)
    # pygame.display.set_caption("minimal program")






# define a main function
def main():
    #constants representing colours
    BLACK_COLOR = (0,   0,   0  )
    WHITE_COLOR = (255, 255,  255  )
    BROWN_COLOR = (125, 102, 18 )
    LIGHTBROWN_COLOR = (240, 222, 141)
    ORANGE_COLOR = (230, 135, 3)
    TRANSPARENT = (0, 0, 0, 0)

    #constants representing the different resources
    WHITE  = -1
    BLACK = 1
    BROWN = 2
    LIGHTBROWN = 3
    SELECTED= 4
    EMPTY = 0

    #a dictionary linking resources to colours
    colours =   {
                    BLACK  : BLACK_COLOR,
                    WHITE : WHITE_COLOR,
                    BROWN : BROWN_COLOR,
                    LIGHTBROWN : LIGHTBROWN_COLOR,
                    EMPTY : TRANSPARENT,
                    SELECTED : ORANGE_COLOR
                    
                }

    #a list representing our background board
    tileMap = [
                [BROWN, LIGHTBROWN, BROWN],
                [LIGHTBROWN, BROWN, LIGHTBROWN],
                [BROWN,  LIGHTBROWN, BROWN]
            ]

    #a list representing the tokens(the actual board to work with)
    tileMapContent = [
                [BLACK, BLACK, BLACK],
                [BLACK, EMPTY, WHITE],
                [WHITE,  WHITE, WHITE]
            ]
    board=Board()
    while True:
        #get all the user events
        for event in pygame.event.get():
            #if the user wants to quit
            if event.type == QUIT:
                #and the game and close the window
                pygame.quit()
                sys.exit()
            #left mouse button event (Select)
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT:
                #get field position
                selectedX, selectedY = board.get_FieldPos()
                if (tileMapContent[selectedY][selectedX] == board.CURRENTPLAYER):
                    #when no field is selected
                    if not board.isSelected(SELECTED, tileMapContent):
                        #when field is empty
                        if not board.isEmpty(selectedX,selectedY):
                            board.selectToken(selectedX, selectedY)
                            possibleMoves, possibleKills = board.getPossibleMoves(selectedX, selectedY)
                            print("Possible Moves: ",possibleMoves)
                            print("Possible Kills[source/target]:", possibleKills)
                    else:
                        tileMapContent[TEMPPOSY][TEMPPOSX] = TEMPCOLOR
            #right mouse button event(move token)
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == RIGHT:
                #when no field is selected
                if isSelected(SELECTED, tileMapContent):
                    toMoveX, toMoveY = get_FieldPos()
                    toMoveYX=[toMoveY,toMoveX]
                    if(toMoveYX in possibleMoves):
                        if isEmpty(toMoveX, toMoveY):
                            moveToken(toMoveX, toMoveY)
                            CURRENTPLAYER = getReverseColor(CURRENTPLAYER)

                            print(CURRENTPLAYER)
                    for sublist in possibleKills:
                        if sublist[0] == toMoveYX:
                            if isEmpty(toMoveX, toMoveY): 
                                moveToken(toMoveX, toMoveY)
                                killToken(sublist[1])
                                CURRENTPLAYER = getReverseColor(CURRENTPLAYER)
                                print("Current Player: ",CURRENTPLAYER)
            pygame.display.update()

        if (CURRENTPLAYER == 1):
            print("computer, it's your turn")
            startNode = Node(tileMapContent,CURRENTPLAYER,0)
            gameTree=startNode.createTree(5)
            bestLeaf,leafScore = startNode.ab_prun(gameTree, False, -100, 100)
            nextMoveNode = bestLeaf.getNextMove()
            tileMapContent = nextMoveNode.board
            CURRENTPLAYER = -1
                            

    # #call drawBoard
    drawBoard()
    # #update the display
    # pygame.display.update()

    
    