import pygame, sys
from pygame.locals import *
import collections
from copy import deepcopy
from GameTree import Node


#constants representing colours
BLACK_COLOR = (0,   0,   0  )
WHITE_COLOR = (255, 255,  255  )
BROWN_COLOR = (125, 102, 18 )
LIGHTBROWN_COLOR = (240, 222, 141)
ORANGE_COLOR = (230, 135, 3)
TRANSPARENT = (0, 0, 0, 0)

#constants representing the different resources
WHITE  = -1
BLACK = 1
BROWN = 2
LIGHTBROWN = 3
SELECTED= 4
EMPTY = 0

#a dictionary linking resources to colours
colours =   {
                BLACK  : BLACK_COLOR,
                WHITE : WHITE_COLOR,
                BROWN : BROWN_COLOR,
                LIGHTBROWN : LIGHTBROWN_COLOR,
                EMPTY : TRANSPARENT,
                SELECTED : ORANGE_COLOR
                
            }

#a list representing our background board
tileMap = [
            [BROWN, LIGHTBROWN, BROWN],
            [LIGHTBROWN, BROWN, LIGHTBROWN],
            [BROWN,  LIGHTBROWN, BROWN]
        ]

#a list representing the tokens(the actual board to work with)
tileMapContent = [
            [BLACK, BLACK, BLACK],
            [BLACK, EMPTY, WHITE],
            [WHITE,  WHITE, WHITE]
        ]

class Board():
    """Create the board"""
    def __init__(self,fields=3,tilesize=80,firstPlayer=WHITE):
        #useful game properties
        self.TILESIZE  = tilesize
        self.MAPWIDTH  = fields
        self.MAPHEIGHT = fields
        TEMPCOLOR = EMPTY
        TEMPPOSX = 0
        TEMPPOSY = 0
        #start always with white player
        self.CURRENTPLAYER = firstPlayer
        LEFT = 1
        RIGHT = 3

        pygame.init()
        self.DISPLAYSURF = pygame.display.set_mode((self.MAPWIDTH*self.TILESIZE,self.MAPHEIGHT*self.TILESIZE))

    def isSelected(self,item,L):
        for i in L:
            if item in i:
                return True
        return False

    def isEmpty(self,x,y):
        if (tileMapContent[y][x] == EMPTY):
            return True
        else: 
            return False

    def get_FieldPos(self):
        #get mouse position x and y
        posx, posy = pygame.mouse.get_pos()

        #get filed position
        selectedX = int(posx / self.TILESIZE)
        selectedY = int(posy / self.TILESIZE)
        return selectedX, selectedY

    def selectToken(self,column,row):
        #getPossibleMoves(x,y)
        global TEMPCOLOR 
        TEMPCOLOR = tileMapContent[row][column]
        global TEMPPOSX 
        TEMPPOSX = column
        global TEMPPOSY 
        TEMPPOSY = row
        tileMapContent[row][column] = SELECTED 

    def getTileMap(self):
        return tileMapContent

    def drawBoard(self):
        #loop through each row
        for row in range(self.MAPHEIGHT):
            #loop through each column in the row
            for column in range(self.MAPWIDTH):
                #draw the resource at that position in the tilemap, using the correct colour
                pygame.draw.rect(self.DISPLAYSURF, colours[tileMap[row][column]], (column*self.TILESIZE,row*self.TILESIZE,self.TILESIZE,self.TILESIZE))
                if (tileMapContent[row][column] != EMPTY):
                    pygame.draw.circle(self.DISPLAYSURF, colours[tileMapContent[row][column]], (int(column*self.TILESIZE+self.TILESIZE/2),int(row*self.TILESIZE+self.TILESIZE/2)), 10, 0)

    def moveToken(self,column,row):
        tileMapContent[row][column] = TEMPCOLOR
        tileMapContent[TEMPPOSY][TEMPPOSX] = EMPTY
        pygame.display.update()


    def killToken(self,sublist):
        row = sublist[0]
        column = sublist[1]
        tileMapContent[row][column] = EMPTY
        pygame.display.update()

    def getReverseColor(self,color):
        if color == BLACK:
            return WHITE
        if color == WHITE:
            return BLACK
        else: return EMPTY