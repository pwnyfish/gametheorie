import pygame, sys
from pygame.locals import *
import collections
from copy import deepcopy
from GameTree import Node


#constants representing colours
BLACKCOLOR = (0,   0,   0  )
WHITECOLOR = (255, 255,  255  )
BROWNCOLOR = (125, 102, 18 )
HELLBROWNCOLOR = (240, 222, 141)
ORANGECOLOR = (230, 135, 3)
EMPTYCOLOR = (0, 0, 0, 0)

#constants representing the different resources
WHITE  = -1
BLACK = 1
BROWN = 2
HELLBROWN = 3
SELECTED= 4
EMPTY = 0

#a dictionary linking resources to colours
colours =   {
                BLACK  : BLACKCOLOR,
                WHITE : WHITECOLOR,
                BROWN : BROWNCOLOR,
                HELLBROWN : HELLBROWNCOLOR,
                EMPTY : EMPTYCOLOR,
                SELECTED : ORANGECOLOR
                
            }

#a list representing our background board
tileMap = [
            [BROWN, HELLBROWN, BROWN],
            [HELLBROWN, BROWN, HELLBROWN],
            [BROWN,  HELLBROWN, BROWN]
        ]

#a list representing the tokens(the actual board to work with)
tileMapContent = [
            [BLACK, BLACK, BLACK],
            [BLACK, EMPTY, WHITE],
            [WHITE,  WHITE, WHITE]
        ]

#useful game properties
TILESIZE  = 80
MAPWIDTH  = 3
MAPHEIGHT = 3
TEMPCOLOR = EMPTY
TEMPPOSX = 0
TEMPPOSY = 0
LEFT = 1
RIGHT = 3
#start always with white player
CURRENTPLAYER = WHITE

#set up the display
pygame.init()
DISPLAYSURF = pygame.display.set_mode((MAPWIDTH*TILESIZE,MAPHEIGHT*TILESIZE))

def isSelected(item,L):
    """Is the token selected"""
    for i in L:
        if item in i:
            return True
    return False

def isEmpty(x,y):
    """Check if the field is empty"""
    if (tileMapContent[y][x] == EMPTY):
        return True
    else: 
        return False

def get_FieldPos():
    """Get the current mouse position and convert it to a useful position on the board"""
    #get mouse position x and y
    posx, posy = pygame.mouse.get_pos()

    #get field position
    selectedX = int(posx / TILESIZE)
    selectedY = int(posy / TILESIZE)
    return selectedX, selectedY

def selectToken(x,y):
    """Change the color for the selected token"""
    global TEMPCOLOR 
    TEMPCOLOR = tileMapContent[y][x]
    global TEMPPOSX 
    TEMPPOSX = x
    global TEMPPOSY 
    TEMPPOSY = y
    tileMapContent[y][x] = SELECTED 

def getTileMap():
    """Returns the current board"""
    return tileMapContent

def drawBoard():
    """Draw the board with pygame functions"""
    #loop through each row
    for row in range(MAPHEIGHT):
        #loop through each column in the row
        for column in range(MAPWIDTH):
            #draw the resource at that position in the tilemap, using the correct colour
            pygame.draw.rect(DISPLAYSURF, colours[tileMap[row][column]], (column*TILESIZE,row*TILESIZE,TILESIZE,TILESIZE))
            if (tileMapContent[row][column] != EMPTY):
                pygame.draw.circle(DISPLAYSURF, colours[tileMapContent[row][column]], (int(column*TILESIZE+TILESIZE/2),int(row*TILESIZE+TILESIZE/2)), 10, 0)
    pygame.display.update()

def moveToken(column,row):
    """Visibly move a token and update the board"""
    tileMapContent[row][column] = TEMPCOLOR
    tileMapContent[TEMPPOSY][TEMPPOSX] = EMPTY
    pygame.display.update()


def killToken(sublist):
    """Visible remove a token and update the board"""
    row = sublist[0]
    column = sublist[1]
    tileMapContent[row][column] = EMPTY
    pygame.display.update()

def getReverseColor(color):
    """Reverse the color"""
    if color == BLACK:
        return WHITE
    if color == WHITE:
        return BLACK
    else: return EMPTY

def getPossibleMoves(column,row):
    """Get all possible moves for the human player.
    This functions checks all surrounding fields within board limitations if they are empty or occupied by the other player.
    If the next field is occupied by the other player, it checks if the next adjacent field is empty to jump over the token"""
    possibleMovesList = []
    possibleKillList = []

    pos = []

    if column+1 <= 2:
        if tileMapContent[row][column+1] == EMPTY:
            pos = [row, column+1]
            pos2 = [[row , column],[row, column+1],[]]

            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row][column+1]) == TEMPCOLOR and column+2 <= 2:
            if tileMapContent[row][column+2] == EMPTY:
                #first position is target field and second positon is the field between source and target
                pos = [[row, column+2],[row, column+1]]
                pos2 = [[row , column],[row, column+2],[row, column+1],[]]
                possibleKillList.append(pos)

    if column+1 <= 2 and row+1 <= 2:
        if tileMapContent[row+1][column+1] == EMPTY:
            pos = [row+1, column+1]
            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row+1][column+1]) == TEMPCOLOR and column+2 <= 2 and row+2 <= 2:
            if tileMapContent[row+2][column+2] == EMPTY:
                pos = [[row+2, column+2],[row+1, column+1]]
                possibleKillList.append(pos)

    if row+1 <= 2:
        if tileMapContent[row+1][column] == EMPTY:
            pos = [row+1, column]
            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row+1][column]) == TEMPCOLOR and row+2 <= 2:
            if tileMapContent[row+2][column] == EMPTY:
                pos = [[row+2, column],[row+1, column]]
                possibleKillList.append(pos)

    if column-1 >= 0 and row+1 <=2:
        if tileMapContent[row+1][column-1] == EMPTY:
            pos = [row+1, column-1]
            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row+1][column-1]) == TEMPCOLOR and column-2 >= 0 and row+2 <= 2:
            if tileMapContent[row+2][column-2] == EMPTY:
                pos = [[row+2, column-2],[row+1, column-1]]
                possibleKillList.append(pos)

    if column-1 >= 0:
        if tileMapContent[row][column-1] == EMPTY:
            pos = [row, column-1]
            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row][column-1]) == TEMPCOLOR and column-2 >= 0:
            if tileMapContent[row][column-2] == EMPTY:
                pos = [[row, column-2],[row, column-1]]
                possibleKillList.append(pos)

    if column-1 >= 0 and row-1 >=0:
        if tileMapContent[row-1][column-1] == EMPTY:
            pos = [row-1, column-1]
            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row-1][column-1]) == TEMPCOLOR and column-2 >=0 and row-2 >= 0:
            if tileMapContent[row-2][column-2] == EMPTY:
                pos = [[row-2, column-2],[row-1, column-1]]
                possibleKillList.append(pos)

    if row-1 >=0:
        if tileMapContent[row-1][column] == EMPTY:
            pos = [row-1, column]
            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row-1][column]) == TEMPCOLOR and row-2 >= 0:
            if tileMapContent[row-2][column] == EMPTY:
                pos = [[row-2, column],[row-1, column]]
                possibleKillList.append(pos)

    if column+1 <= 2 and row-1 >=0:
        if tileMapContent[row-1][column+1] == EMPTY:
            pos = [row-1, column+1]
            possibleMovesList.append(pos)
        elif getReverseColor(tileMapContent[row-1][column+1]) == TEMPCOLOR and column+2 <= 2 and row-2 >= 0:
            if tileMapContent[row-2][column+2] == EMPTY:
                pos = [[row-2, column+2],[row-1, column+1]]
                possibleKillList.append(pos)

    return possibleMovesList, possibleKillList


startNode = Node(tileMapContent,CURRENTPLAYER,0)

"""Main function to start the game"""
while True:

    if CURRENTPLAYER==-1:
        #get all the user events
        for event in pygame.event.get():
            #if the user wants to quit
            if event.type == QUIT:
                #and the game and close the window
                pygame.quit()
                sys.exit()

            #left mouse button event (Select)
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT:
                #get field position
                selectedX, selectedY = get_FieldPos()
                if (tileMapContent[selectedY][selectedX] == CURRENTPLAYER):
                    #when no field is selected
                    if not isSelected(SELECTED, tileMapContent):
                        #when field is empty
                        if not isEmpty(selectedX,selectedY):
                            selectToken(selectedX, selectedY)
                            possibleMoves, possibleKills = getPossibleMoves(selectedX, selectedY)
                            print("Possible Moves: ",possibleMoves)
                            print("Possible Kills[source/target]:", possibleKills)
                    else:
                        tileMapContent[TEMPPOSY][TEMPPOSX] = TEMPCOLOR
            #right mouse button event(move token)
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == RIGHT:
                #when no field is selected
                if isSelected(SELECTED, tileMapContent):
                    toMoveX, toMoveY = get_FieldPos()
                    toMoveYX=[toMoveY,toMoveX]
                    if(toMoveYX in possibleMoves):
                        if isEmpty(toMoveX, toMoveY):
                            moveToken(toMoveX, toMoveY)
                            CURRENTPLAYER = getReverseColor(CURRENTPLAYER)

                            print(CURRENTPLAYER)
                    for sublist in possibleKills:
                        if sublist[0] == toMoveYX:
                            if isEmpty(toMoveX, toMoveY): 
                                moveToken(toMoveX, toMoveY)
                                killToken(sublist[1])
                                CURRENTPLAYER = getReverseColor(CURRENTPLAYER)


    #call drawBoard
    #update the display
    drawBoard()
    #computer part
    if (CURRENTPLAYER == 1):
        print("Computer's Turn")
        startNode = Node(tileMapContent,CURRENTPLAYER,0)
        gameTree=startNode.createTree(5)
        bestLeaf,leafScore = startNode.ab_prun(gameTree, False, -100, 100)
        nextMoveNode = bestLeaf.getNextMove()
        tileMapContent = nextMoveNode.board
        print("Player's Turn")
        CURRENTPLAYER = -1
                            

