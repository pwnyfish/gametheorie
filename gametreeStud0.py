import math
from copy import deepcopy
#from anytree import Node,RenderTree
# http://www.python-kurs.eu/deep_copy.php


#class GameTreeAlgorithms:

    
#  Minimax-Verfahren:
#  Spiebaum wird als verschachtelte Listenstruktur �bergeben 
def minimax1(l):

    if (type(l) == list):

        a = -100
        n = len(l)
        print(range(n))
        for i in range(n):
             print(i)
             f = - minimax1(l[i])
             if f > a:
                 a = f
        return a
    else:
        return l


#Alpha-Beta prunning:
#random bananas
tree1 = [[[-7,-8,16],[-8,-3,-13],[14,-15,10]],[[-9,17,-15],[-17,14,2],[14,19,-4]],[[4,-7,5],[17,11,-5],[14,-11,15]]] #result should be 14
tree2 = [[[7,6,10],[7,-6,-19],[15,19,-2]],[[18,-11,3],[17,-4,4],[0,7,-2]],[[17,12,2],[3,-14,12],[7,16,-12]]] #result should be 12

max=False #Has to be initialized false because stepping into the first Max node will invert it
a=-100
b=100
v=0
def ab_prun(tree,max,a,b):
    #check if not leaf
    if (type(tree) == list):
        n = len(tree)
        #print(range(n))
        max = not max

        #max node
        if(max==True):
            for i in range(n):
                # print(tree[i])
                # print(max)            
                # print("do max turn")
                #go down to next level
                v=ab_prun(tree[i],max,a,b)
                if(a>=b):
                    #cut off
                    return a
                if(a<=v):
                    a=v
            return a

        #if min node
        if(max==False):
            for i in range(n):
                # print(tree[i])
                # print(max)   
                # print("do min turn")
                #go down to next level
                v=ab_prun(tree[i],max,a,b)
                if(a>=b):
                    #cut off
                    return b
                if(b>=v):
                    b=v
            return b
    else:
        #return leaf value
        return tree

x=ab_prun(tree2,max,a,b)
print("END OF ALGORITHM:")
print(x)

# root = Node("root")
# Node("max",parent=root)
# print(root)