from copy import deepcopy
WHITE  = -1
BLACK = 1
GAGIBROWN = 2
HELLGAGIBROWN = 3
SELECTED= 4
EMPTY = 0
class Node(object):

    def __init__(self, board,player,score):
        self.parent = None
        self.board = board
        self.score = score
        self.value = score
        self.alpha = 0
        self.beta = 0
        self.player = player
        self.children = []
        self.allMovement = []
        self.treeBranch = []

    def getTreeBranch(self,branch=[]):
        """Show a list of all last moves"""
        #show the branch to the currently best possible outcome
        self.treeBranch=branch
        if self.parent != None:
            branch.append(self.parent)
            self.parent.getTreeBranch(branch)
        
        return branch


    def getNextMove(self):
        """Get the Next move"""
        #get the first/last element of the tree branch
        nextNode=self.getTreeBranch()
        return nextNode[-2]

    def add_child(self, obj):
        """Add a new child node"""
        self.children.append(obj)
        obj.parent=self


    def print(self, level=0):
        """Print out the board"""
        print ('\t' * level + repr(level)+'/'+repr(self.board)+'/score='+repr(self.score))
        for child in self.children:
            child.print(level+1)

    def mprint(self, level=0):
        """A prettier, but more loaded way to print out the board"""
        print ('\t' * level + repr(level)+'/'+repr(self.board[0]))
        print ('\t' * level + repr(level)+'/'+repr(self.board[1]))
        print ('\t' * level + repr(level)+'/'+repr(self.board[2]))
        print('\t'*level + '---------------')
        print('\t'*level + "Current Player: " + repr(self.player))
        print('\t'*level + "Movement List[source,target,kill]:")
        for i,j in enumerate(self.allMovement):
            print('\t'*level + repr(self.allMovement[i]))
        print('\t'*level + '---------------')
        for child in self.children:
            child.mprint(level+1)             

    def calcNewMap(self,gameMap,moves,player):
        """calculate new map"""
        source = moves[0]
        target = moves[1]
        kill = moves[2]
        newGameMap=deepcopy(gameMap)
        newGameMap[source[0]][source[1]] = EMPTY
        newGameMap[target[0]][target[1]] = player
        if kill:
            newGameMap[kill[0]][kill[1]] = EMPTY
        return newGameMap

    def createTree(self,maxDepth):
        """Create tree"""
        gameTree=[]
        maxDepth-=1
        self.treeBranch.append(self)
        moves=self.getAllMoves(self.board,self.player)
        nextPlayer=self.getReverseColor(self.player)
        for sublist in moves:
            for i in sublist:
                newMap=self.calcNewMap(self.board,i,self.player)
                score=self.calcBoardScore(newMap)
                newNode=Node(newMap,nextPlayer,score)
                self.add_child(newNode)
                if not(newNode.isLeaf() or maxDepth==0):
                    gameTreeChild=newNode.createTree(maxDepth)
                    gameTree.append(gameTreeChild)
                else:

                    gameTree.append(newNode)

        return gameTree

    def ab_prun(self,tree,max,a,b):
        """Alpha-Beta Pruning function"""
        #check if not leaf
        if (type(tree) == list):
            n = len(tree)
            max = not max

            #max node
            if(max==True):
                for i in range(n):
                    #go down to next level
                    node,v=self.ab_prun(tree[i],max,a,b)
                    if(a>=b):
                        #cut off
                        return node,a
                    if(a<=v):
                        a=v
                return node,a

            #if min node
            if(max==False):
                for i in range(n):
                    #go down to next level
                    node,v=self.ab_prun(tree[i],max,a,b)
                    if(a>=b):
                        #cut off
                        return node,b
                    if(b>=v):
                        b=v
                return node,b
        else:
            #return leaf value
            return tree,tree.value

    def getAllMoves(self,gameTree,currentPlayer):
        """Function to get all moves that can be done on the board by the current player"""
        allMoves=[]
        for row, sublist in enumerate(gameTree):
            if currentPlayer in sublist:
                for column, item in enumerate(sublist):
                    if currentPlayer == item:
                        everyMove=self.getPossibleMoves(gameTree,column,row,currentPlayer)
                        if everyMove:
                            allMoves.append(everyMove)                            
        self.allMovement=deepcopy(allMoves)
        return allMoves

    def calcScore(self):
        """Calculate the score for the current Node.
        This function only summs up all tokesn on the board and generates the score 
        out of that"""
        score=0
        for row in self.board:
            for column in row:
                score+=column
        return score

    def calcBoardScore(self,board):
        """Calculate the score for the current board.
        This function only summs up all tokesn on the board and generates the score 
        out of that"""
        score=0
        for row in board:
            for column in row:
                score+=column
        return score


    def getPossibleMoves(self,gameTree,column,row,player):
        """Get every possible move for the current token"""
        everyMove = []
        if column+1 <= 2:
            if gameTree[row][column+1] == EMPTY:
                move = [[row , column],[row, column+1],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row][column+1]) == player and column+2 <= 2:
                if gameTree[row][column+2] == EMPTY:
                    move = [[row , column],[row, column+2],[row, column+1]]
                    everyMove.append(move)

        if column+1 <= 2 and row+1 <= 2:
            if gameTree[row+1][column+1] == EMPTY:
                move = [[row , column],[row+1, column+1],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row+1][column+1]) == player and column+2 <= 2 and row+2 <= 2:
                if gameTree[row+2][column+2] == EMPTY:
                    move = [[row , column],[row+2, column+2],[row+1, column+1]]
                    everyMove.append(move)

        if row+1 <= 2:
            if gameTree[row+1][column] == EMPTY:
                move = [[row , column],[row+1, column],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row+1][column]) == player and row+2 <= 2:
                if gameTree[row+2][column] == EMPTY:
                    move = [[row , column],[row+2, column],[row+1, column]]
                    everyMove.append(move)

        if column-1 >= 0 and row+1 <=2:
            if gameTree[row+1][column-1] == EMPTY:
                move = [[row , column],[row+1, column-1],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row+1][column-1]) == player and column-2 >= 0 and row+2 <= 2:
                if gameTree[row+2][column-2] == EMPTY:
                    move = [[row , column],[row+2, column-2],[row+1, column-1]]
                    everyMove.append(move)

        if column-1 >= 0:
            if gameTree[row][column-1] == EMPTY:
                move = [[row , column],[row, column-1],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row][column-1]) == player and column-2 >= 0:
                if gameTree[row][column-2] == EMPTY:
                    move = [[row , column],[row, column-2],[row, column-1]]
                    everyMove.append(move)

        if column-1 >= 0 and row-1 >=0:
            if gameTree[row-1][column-1] == EMPTY:
                move = [[row , column],[row-1, column-1],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row-1][column-1]) == player and column-2 >=0 and row-2 >= 0:
                if gameTree[row-2][column-2] == EMPTY:
                    move = [[row , column],[row-2, column-2],[row-1, column-1]]
                    everyMove.append(move)

        if row-1 >=0:
            if gameTree[row-1][column] == EMPTY:
                move = [[row , column],[row-1, column],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row-1][column]) == player and row-2 >= 0:
                if gameTree[row-2][column] == EMPTY:
                    move = [[row , column],[row-2, column],[row-1, column]]
                    everyMove.append(move)

        if column+1 <= 2 and row-1 >=0:
            if gameTree[row-1][column+1] == EMPTY:
                move = [[row , column],[row-1, column+1],[]]
                everyMove.append(move)
            elif self.getReverseColor(gameTree[row-1][column+1]) == player and column+2 <= 2 and row-2 >= 0:
                if gameTree[row-2][column+2] == EMPTY:
                    move = [[row , column],[row-2, column+2],[row-1, column+1]]
                    everyMove.append(move)

        return everyMove

    def getReverseColor(self,color):
        """Reverse the current color. Used to switch player"""
        if color == BLACK:
            return WHITE
        if color == WHITE:
            return BLACK
        else: return EMPTY

    def isLeaf(self):
        """No more possible moves"""
        moves=self.getAllMoves(self.board,self.player)

        def isListEmpty(inList):
            if isinstance(inList,list):
                return all(map(isListEmpty, inList))
            return False
        return isListEmpty(moves)